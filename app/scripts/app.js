// configure the angular.js app (dependencies, config, routes)
(function($ng) {
  'use strict';
  var crezeoApp = $ng.module('milky-way', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ui.router',
    'ui.bootstrap'
  ]);

  crezeoApp.config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
    function($stateProvider, $urlRouterProvider, $locationProvider) {

      $stateProvider
        .state('home', {
          url: "/",
          templateUrl: "themes/default/views/home.html"
        });

      $locationProvider.html5Mode((document.location.port == 9000) || document.location.host.match(/local/) ? false : true);
      $locationProvider.hashPrefix('!');
    }]);

}(window.angular));
