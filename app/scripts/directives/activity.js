(function ($ng) {
  'use strict';

  var directive = ['$rootScope', '$compile', '$http', '$templateCache', '$sce', '$timeout', 'pubsub', 'api', '$state','$uibModal', function ($rootScope, $compile, $http, $templateCache, $sce, $timeout, pubsub, api, $state, $uibModal) {
    /*

     crActivity
     ======
     > Launch the right template and methods for an activity


     Accessible methods in scope
     ------
     ** openSinglePost ** Old Auchan method to open sidebar (remove as soon as the code is merged)
     ** saveItemView ** Save information that the activity target has been viewed (active only for status)

     Local methods
     ------
     ** getTemplate **  Return a promise with the right template for the current activity

     PubSub Events
     ------
     ### Subscribe
     ** updateElement[target_id][targetTypeName] **  Triggered when an activity has to be updated, get the activity
     ** refreshActivities **  Wait for a manual refresh, recompile activity

     */

    var getTemplate = function (contentType, subjectType) {
      var templateLoader,
        templateMap = {
          Post: 'bower_components/' + MilkyWayConf.theme + '/app/views/activities/post.html',
          PostStructure: 'bower_components/' + MilkyWayConf.theme + '/app/views/activities/post.html',
          User: 'bower_components/' + MilkyWayConf.theme + '/app/views/activities/user.html',
          Community: 'bower_components/' + MilkyWayConf.theme + '/app/views/activities/follow.html',
          Status: 'bower_components/' + MilkyWayConf.theme + '/app/views/activities/status.html'
        };

      var templateUrl = templateMap[subjectType];
      templateLoader = $http.get(templateUrl, {cache: $templateCache});

      return templateLoader;

    };

    var linker = function (scope, element, attrs) {

      var loader = getTemplate(scope.activity.target_type, scope.activity.subject_type);
      scope.gridClass = attrs.gridclass;
      scope.parentItemId = attrs.parentItemId;

      // DEPRECATED
      //var openSideBarDebounced = function (slug, targetType, commentClick) {
      //  pubsub.publish('loadSideTemplateModal', {targetType: targetType, slug: slug, commentClick: commentClick});
      //};
      //
      //scope.openSideBar = _.debounce(openSideBarDebounced, 10);


      loader.then(function (response) {
//        console.log('OOK', response.data);

        // append html of the template to the directive element
        element.html(response.data);
      }).finally(function (response) {

          // ********* Declarations *********
          // get the activity target which is the item that will be displayed
          var targetTypeName = scope.activity.subject_type;
          if (targetTypeName == 'Post') {
            targetTypeName = 'PostStructure'
          }
          var subscribeEventName = 'updateElement' + scope.activity.subject_id + targetTypeName;
          // pass the current user
          scope.$user = $rootScope.$user;
          // get the activity target which is the item that will be displayed
          scope.item = scope.activity.subject;
          scope.target = scope.activity.target;
          scope.owner = scope.activity.owner;


          if (scope.item && scope.item.embed && scope.item.embed.html && scope.item.embed.html.toString()) {
            scope.item.embed.html = $sce.trustAsHtml(scope.item.embed.html.toString());
          }


          pubsub.subscribe('refreshActivities', function () {
            $compile(element.contents())(scope); //<---- recompilation
          }, scope);

          if(scope.activity && scope.activity.subject && scope.activity.subject.sharable){
            var subscribeEventNameSharable = 'updateElement' + scope.activity.subject.sharable.id + scope.activity.subject.sharable_type;
            pubsub.subscribe(subscribeEventNameSharable, function (event, element_id, classname) {
              api.activities.get(scope.activity.id + '?target_type=' + scope.activity.target_type + '&target_id=' + scope.activity.target_id).then(function (response) {
                scope.item = response.data.subject;
              });
            },scope);
          }

          pubsub.subscribe(subscribeEventName, function (event, element_id, classname) {
            if (classname == 'PostStructure') {
              classname = 'Post';
            }
            api.activities.get(scope.activity.id + '?target_type=' + classname + '&target_id=' + element_id).then(function (response) {
              scope.item = response.data.subject;
            });
          }, scope);

          // ********* Launchers *********
          // replace template url by its content and call scope
          $compile(element.contents())(scope);

        });
    };

    var ctrl = ['$scope', '$rootScope', '$http', '$state', 'pubsub', function ($scope, $rootScope, $http, $state, pubsub) {
      $scope.currentStateName = $state.current.name
      $scope.saveItemView = function (itemId, itemType) {
        if (itemId && itemType) {
          if (itemType == 'Status') {
            $http.post(window.API_URL + "/api/v1/statuses/" + itemId + "/view");
          } else {
//          $http.post(window.API_URL + "/api/v1/suggestion_actions", {bloc_type: blocType, action: 'viewed', field: scope.suggestion.field, bloc_id: scope.suggestion.bloc_id });
          }
        }
      };
      $scope.displayCommentForm = function () {
        $scope.item.displayCommentForm = !$scope.item.displayCommentForm;
        if ($scope.item.displayCommentForm) {
          var targetTypeName = $scope.activity.subject_type;
          if (targetTypeName == 'Post') {
            targetTypeName = 'PostStructure';
          }
          $timeout(function () {
            pubsub.publish("focusOn", ("commentForm" + targetTypeName + $scope.item.id));
          }, 100);
        }
      };
//      $scope.disableParent = function () {
//      }


    }];

    return {
      replace: true,
      restrict: 'E',
      scope: {
        activity: '=',
        pinned: '=',
        hideAction: '=',
        hideComment: '='
      },
      link: linker,
      controller: ctrl
    };
  }];


  $ng.module('crezeo-activities').directive('crActivity', directive);

}(window.angular));
