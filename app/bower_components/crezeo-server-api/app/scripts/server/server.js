(function($ng) {
  'use strict';
  var svc = ['$http', '$q', function($http, $q) {
    //TODO: base from configuration
    var base = window.API_URL + '/api/v1/';

    function _get() {
      var cache;
      if (!arguments || (arguments.length <= 1)) {
        return $q.defer().reject('invalid get request.  no argument present for resource.');
      }
      var resource = arguments[0];
      var url = base + resource;
      var arglen = arguments.length;
      if (arglen > 1) {
        for (var i = 1; i < arglen; i++) {
          var arg = arguments[i];
          if (arg && arg._isApiCache) {
            cache = arg;
          } else {
            url += ('/' + arguments[i]);
          }
        }
      }

      return $http.get(url, {cache: cache});
    }

    function _list(resource, cache) {
      return $http.get(base + resource, {cache: cache});
    }

    // Params is an object.
    // If params is null or empty, redirect to 'list'.
    function _search(resource, addBase, params, cache) {
      if (!params) {
        return _list(resource);
      }

      var url = base + resource + (addBase ? (addBase + '?') : '?') ;

      for (var p in params) {
        if (params.hasOwnProperty(p)) {
          var val = params[p];
          if (val instanceof Array) {
            for (var i = 0; i < val.length; i++) {
              url += (p + '[]' + '=' + val[i] + '&');
            }
          } else {
            url += (p + '=' + val + '&');
          }
        }
      }
      //remove trailing ampersand
      if (url.lastIndexOf('&') === (url.length - 1)) {
        url = url.substring(0, url.length - 1);
      }

      return $http.get(url, {cache: cache});
    }

    // If child is present, we want to create a representation based on the child.  e.g. POST /institution/1/follow
    function _create(resource, obj, child, childObj) {
      var cp;
      if (child) {
        if (childObj) {
          cp = $ng.copy(childObj);
          delete cp._resource; // remote api is not happy with unknown field
          return $http.post(base + resource + '/' + obj + '/' + child, cp);
        }
        else if (!obj.id && (typeof obj !== 'number')) {
          cp = $ng.copy(obj);
          delete cp._resource; // remote api is not happy with unknown field
          return $http.post(base + resource + '/' + child, cp);
        }
        else {
          return $http.post(base + resource + '/' + obj + '/' + child);
        }

      } else {
        cp = $ng.copy(obj);
        delete cp._resource; // remote api is not happy with unknown field
        return $http.post(base + resource, cp);
      }
    }

    function _update(resource, obj) {
      var cp = $ng.copy(obj);
      delete cp._resource; // remote api is not happy with unknown field
      return $http.put(base + resource + '/' + cp.id, cp);
    }

    function _remove(resource, id) {
      return $http.delete(base + resource + '/' + id);
    }

    return {
      get: _get,
      list: _list,
      search: _search,
      create: _create,
      update: _update,
      remove: _remove
    };

  }];

  $ng.module('crezeo-server-api')
    .factory('server', svc);

}(window.angular));
