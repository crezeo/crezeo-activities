(function ($ng) {
  'use strict';

  var directive = ['$rootScope', '$uibModal', '$log', '$filter', function($rootScope, $uibModal, $log, $filter) {

    var $translate = $filter('translate');

    return {
      restrict: 'E',
      scope: {
        data: '=',
        activity: '=',
        type:'=',
        customClass:'@'
      },
      templateUrl: function(elem, attrs) {
        var _templateMap = {
          header : 'bower_components/' + MilkyWayConf.theme + '/app/views/aggregate/header.html',
          body : 'bower_components/' + MilkyWayConf.theme + '/app/views/aggregate/body.html'
        };
        return _templateMap[attrs.type];
      },
      controller: ['$scope', 'userSession', function($scope, userSession) {

        var highestCount = 0;
        $scope.aggUsers = "";
        $scope.verb = "";
        $scope.target = "";
        $scope.message = "";

        $scope.current_user = userSession.user();


        $scope.$watch("activity", function (newVal, oldVal) {
          if($scope.activity != undefined) {
            makeAggregateText();
          }
        });


        $scope.closeModal = function(){
          closeInviteModal();
        };
        function closeInviteModal(){
          if($scope.detailAggregateInstance){
            $scope.detailAggregateInstance.close(true);
            $scope.detailAggregateInstance = undefined;
          }
        }
        $scope.openModal = function(){
          if(!$scope.detailAggregateInstance){
            $scope.detailAggregateInstance = $uibModal.open({
              templateUrl: 'bower_components/' + MilkyWayConf.theme + '/app/views/aggregate/modal.html',
              windowClass: 'default-modal aggregate-modal',
              scope: $scope
            });

            $scope.detailAggregateInstance.result.finally(function() {
              $scope.detailAggregateInstance = undefined;
            });
          }
        }

        function makeAggregateText() {
          angular.forEach($scope.data.action_type_count, function (value, key) {
            if (value > highestCount) {
              highestCount = value;
              $scope.mainType = key;
            }
          });


          var ind = 0;
          angular.forEach($scope.data.action_users[$scope.mainType], function (value, key) {
            if ($scope.data.action_users[$scope.mainType].length > 2) {
              // Tooltip case
              if (ind == 0) {
                $scope.aggUsers = $scope.aggUsers + ' <a ui-sref="member.activities({slug: \'' + value.slug + '\'})" ng-click="$event.stopPropagation()">' + value.nickname + '</a>' + ', ';
              } else if (ind == 1) {
                $scope.aggUsers = $scope.aggUsers + '<a ui-sref="member.activities({slug: \'' + value.slug + '\'})" ng-click="$event.stopPropagation()">' + value.nickname + '</a>' + ' et ';
              } else if (ind == 2){
                if(($scope.data.action_users[$scope.mainType].length - 2) > 1) {
                  $scope.aggUsers =  $scope.aggUsers + '<a href="" ng-click="$event.stopPropagation();openModal()">' + ($scope.data.action_users[$scope.mainType].length - 2) + ' autres personnes' + '</a>';
                } else {
                  $scope.aggUsers = $scope.aggUsers + '<a href="" ng-click="$event.stopPropagation();openModal()">' + ($scope.data.action_users[$scope.mainType].length - 2) +  ' autre personne' + '</a>';
                }
              } else {
                $scope.aggUsers = $scope.aggUsers;
              }
            } else {
              // classic case
              if (ind == 0 && $scope.data.action_users[$scope.mainType].length == 2) {
                $scope.aggUsers = $scope.aggUsers + '<a ui-sref="member.activities({slug: \'' + value.slug + '\'})" ng-click="$event.stopPropagation()">' + value.nickname + '</a>' + ' et ';
              } else {
                $scope.aggUsers = $scope.aggUsers + '<a ui-sref="member.activities({slug: \'' + value.slug + '\'})" ng-click="$event.stopPropagation()">' + value.nickname + '</a>';
              }
            }

            ind = ind + 1;
          });

          switch ($scope.mainType) {
            case 'comment':
              if ($scope.data.action_users[$scope.mainType].length >= 2) {
                $scope.verb = " " + $translate('aggregation.have_commented_plur');
              }else{
                $scope.verb = " " + $translate('aggregation.have_commented');
              }
              break;
            case 'like':
              if ($scope.data.action_users[$scope.mainType].length >= 2) {
                $scope.verb = " " + $translate('aggregation.have_liked_plur');
              }else{
                $scope.verb = " " + $translate('aggregation.have_liked');
              }
              break;
            case 'recommend':
              if ($scope.data.action_users[$scope.mainType].length >= 2) {
                $scope.verb = " " + $translate('aggregation.have_recommended_plur');
              }else{
                $scope.verb = " " + $translate('aggregation.have_recommended');
              }
              break;
            case 'follow':
              if ($scope.data.action_users[$scope.mainType].length >= 2) {
                $scope.verb = " " + $translate('aggregation.have_followed_plur');
              }else{
                $scope.verb = " " + $translate('aggregation.have_followed');
              }
              break;
            default:
              $log.error('No verb for ' + $scope.mainType);
          }


          switch ($scope.activity.subject_type) {
            case 'Status':
              if($scope.current_user.id == $scope.activity.owner_id){
                $scope.target = " " + $translate('aggregation.your_status');
              }else{
                $scope.target = " " + $translate('aggregation.status_of');
              }
              break;
            case 'Post':
              if($scope.current_user.id == $scope.activity.owner_id) {
                $scope.target = " " + $translate('aggregation.your_post');
              }else{
                $scope.target = " " + $translate('aggregation.post_of');
              }
              break;
            case 'Community':
                $scope.target = " " + $translate('aggregation.group');
              break;
            case 'User':
              $scope.target = " " + $translate('aggregation.user');
              break;
            default:
              $log.error('No target for ' + $scope.activity.subject_type);
          }

          $scope.message = $scope.aggUsers + ' ' +  $scope.verb + ' ' +  $scope.target;

        }

      }]

    };

  }];


  $ng.module('crezeo-activities').directive('aggregationMessage', directive);

}(window.angular));
