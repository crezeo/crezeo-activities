/// The pubsub service is an abstraction for rootScope broadcaster/emitter.
/// It's primary purpose is to prevent the need of passing rootScope into
/// controllers.
(function($ng) {
  'use strict';
  var svc = ['$rootScope', '$log', function($rootScope, $log) {

    function _publish(name, args) {
      $rootScope.$broadcast.apply($rootScope, arguments);
    }

    // passing in the $scope will automatically deregister the listener when the scope is destroyed
    function _subscribe(name, listener, $scope) {
      var deregister = $rootScope.$on(name, listener);
      if ($scope) {
        $scope.$on('$destroy', deregister);
      }else{
        $log.warn('scope is not passed into pubsub subscription; possible memory leak.', name);
      }
      return deregister;
    }

    return {
      publish: _publish,
      subscribe: _subscribe
    };

  }];

  $ng.module('crezeo-server-api').factory('pubsub', svc);

}(window.angular));
