// this service works with the form_field and model_compile directives to provide generic error mapping
(function($ng, _) {
  'use strict';

  var svc = [function() {
    var _errors;

    function init(errors) {
      _errors = errors;
      angular.forEach(errors, function (v, k) {
        if (typeof(v) == 'object') {
          angular.forEach(v, function (v1, k1) {
            if(k1.indexOf('.') > -1){
              k+k1
              _errors[k+'.'+k1] = v1
            }
          })
        }
      })
    }

    function get(path) {
      var parts = path.split('.');
      var target = _errors;
      for (var p = 0; p < parts.length; p++) {
        target = target[parts[p]];
        if (!target) {
          target = _errors[path]
          if(!target){
            return undefined;
          }
        }
      }
      return target;
    }

    function set(path, value) {
      _errors[path] = value;
    }

    return {
      init: init,
      get: get,
      set: set
    };

  }];

  $ng.module('crezeo-server-api').factory('errors', svc);

}(window.angular, window._));
