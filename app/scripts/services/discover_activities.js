// Manage permissions for current user
(function ($ng) {
  'use strict';
  var svc = ['api', '$log', '$q', function (api, $log, $q) {
    var activities;
    var last_activities;

    function _get(tag_id, force_reload, filterName, page) {
      var d = $q.defer();
      page = page || undefined;
      tag_id = tag_id || undefined;
      filterName = filterName || 'all';
      force_reload = force_reload || undefined;
      check_activities(tag_id, force_reload, filterName, page).then(function (response) {
        set(response);
        d.resolve(activities);
      });
      return d.promise;
    }

    function set(response) {
      activities = response;
    }
    function check_activities(tag_id, force_reload, filterName, page) {

      var i = $q.defer();
      if(force_reload){activities = undefined}
      //if no activities defined get on api
      //if activities is set, check with first activities id is equal of last activities actually given by api.
      // When change new request to get activities is sent.
      if (activities == undefined) {
        get_activities(tag_id, filterName, page).then(function (response) {
          i.resolve(response);
        });
      }
      else {
//        get_last_activity_id(tag_id, filterName).then(function (response) {
//          if (activities && activities[0] && activities[0]["id"] == response) {
//            i.resolve(activities);
//          } else {
            get_activities(tag_id, filterName, page).then(function (response) {
              i.resolve(response);
            });
//          }
//        });
      }
      return i.promise;
    }

    function get_activities(tag_id, filterName, page) {
      var query = $q.defer();
      var apiQuery = 'discover';

      if (tag_id && page) {
        apiQuery = apiQuery + '?tag_id=' + tag_id + '&filter_name=' + filterName + '&page=' + page;
      } else if (tag_id) {
        apiQuery = apiQuery + '?tag_id=' + tag_id + '&filter_name=' + filterName;
      } else if (page) {
        apiQuery = apiQuery + '?page=' + page;
      }

      api.activities.get(apiQuery)
        .then(function(response){
          query.resolve(response.data);
        });
      return query.promise;
    }

    function get_last_activity_id(tag_id, filterName) {
      var query = $q.defer();
      var apiQuery = tag_id ? 'discover?tag_id=' + tag_id + '&filter_name=' + filterName : 'discover';
      api.activities.get(apiQuery)
        .then(function (response) {
          query.resolve(response.data);
        });
      return query.promise;
    }


    return {
      get: _get
    };
  }];

  $ng.module('crezeo-activities').factory('discoverActivities', svc);
}(window.angular));
