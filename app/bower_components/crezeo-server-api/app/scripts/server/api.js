/// Provides rest hooks into models and resources giving us a single service for communication with the server.
(function($ng) {
  'use strict';
  var svc = ['$q', '$log', '$cacheFactory','$rootScope', 'server', 'models', 'pubsub', function($q, $log, $cacheFactory,$rootScope, server, models, pubsub) {
    var api = {}, resourceCacheMap = {};

    ///
    /// prefix api method names with '_' in case these words become reserved in the future.
    ///
    function _get() {
      /*jshint validthis: true */
      var cache = this._cache; // default cache for object; set up in object and stored in object and in resourceCacheMap
      /*jshint validthis: true */
      var args = [this._resource];
      /*jshint validthis: true */
      var cvt = Array.prototype.slice.call(arguments);
      args = args.concat(cvt);

      // check if cache exits for the last child argument
      if(args.length > 1){
        var test = args[args.length-1];
        var childCache = test && resourceCacheMap[test];
        if(childCache){
          // use the child cache; e.g. users/id/media_library where 'media_library' is the child cache
          cache = childCache;
        }
      }
      if(cache){
        args.push(cache);
      }
      return server.get.apply(server, args);
    }

    function _list() {
      /*jshint validthis: true */
      return server.list(this._resource, this._cache);
    }

    function _search(params, addBase) {
      /*jshint validthis: true */
      return server.search(this._resource, addBase, params, this._cache);
    }

    function _save(child, childObj) {
      /*jshint validthis: true */
      if (this._cache_name) {
        // remove cached items
        resourceCacheMap[this._cache_name].removeAll();
      }
      if (this.id && child) {
        return server.create(this._resource, this.id, child, childObj);
      }
      else if (!this.id && child) {
        return server.create(this._resource, this, child, childObj);
      }
      else if (this.id) {
        return server.update(this._resource, this);
      } else {
        var self = this;
        return server.create(this._resource, this)
          .success(function(data) {
            if (data.id) {
              self.id = data.id;
            }
          });
      }
    }

    function _remove() {
      /*jshint validthis: true */
      if (this._cache_name) {
        // remove cached items.
        resourceCacheMap[this._cache_name].removeAll();
      }
      return server.remove(this._resource, this.id);
    }

    function listenForInvalidate(cacheName){
      // listen to global cache invalidation from other areas of the app
      pubsub.subscribe('cache.invalidate.' + cacheName, function(){
        var rc = resourceCacheMap[cacheName];
        if(rc){
          rc.removeAll();
        }
      }, $rootScope);
    }

    for (var m in models.models) {
      if (models.models.hasOwnProperty(m)) {
        var mod = models.models[m];
        api[m] = mod.extend({
          'save': _save,
          'remove': _remove
        });
        api[m]._resource = mod._resource;
        api[m].get = _get;
        api[m].list = _list;
        api[m].search = _search;
        if (mod._cache_name) {
          // if a cache_name is set up on this object, attach the existing cache or create a new one.
          api[m]._cache = $cacheFactory.get(mod._cache_name) || $cacheFactory(mod._cache_name);
          // allow server service to test if cache is an api cache
          api[m]._cache._isApiCache = true;
          // add cache to the resourceCacheMap to allow save and delete methods to check for cache in api calls.
          resourceCacheMap[mod._cache_name] = api[m]._cache;
          // listen for global cache invalidation from other areas of the app.
          listenForInvalidate(mod._cache_name);
        }

      }
    }

    return api;
  }];

  $ng.module('crezeo-server-api')
    .factory('api', svc);

}(window.angular));
