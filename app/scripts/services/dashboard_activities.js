// Manage permissions for current user
(function ($ng) {
  'use strict';
  var svc = ['api', '$log', '$q', function (api, $log, $q) {
    var activities;
    var last_activities;

    function _get(user_id, force_reload) {
      var d = $q.defer();
      force_reload = force_reload || undefined
      check_activities(user_id, force_reload).then(function (response) {
        set(response);
        d.resolve(activities);
      });
      return d.promise;
    }

    function set(response) {
      activities = response;
    }

    function check_activities(user_id, force_reload) {
      var i = $q.defer();
      if(force_reload){activities = undefined}
      //if no activities defined get on api
      //if activities is set, check with first activities id is equal of last activities actually given by api.
      // When change new request to get activities is sent.
      if (activities == undefined) {
        get_activities(user_id).then(function (response) {
          i.resolve(response);
        });
      }
      else {
        get_last_activity_id(user_id).then(function (response) {
          if (activities && activities[0] && activities[0]["id"] == response) {
            i.resolve(activities);
          } else {
            get_activities(user_id).then(function (response) {
              i.resolve(response);
            });
          }
        });
      }
      return i.promise;
    }

    function get_activities(user_id) {
      var query = $q.defer();
      api.users.get(user_id, 'current_user_activities')
        .then(function(response){
          query.resolve(response.data);
        });
      return query.promise;
    }

    function get_last_activity_id(user_id) {
      var query = $q.defer();
      api.users.get(user_id, 'last_activities')
        .then(function (response) {
          query.resolve(response.data);
        });
      return query.promise;
    }

    return {
      get: _get
    };
  }];

  $ng.module('crezeo-activities').factory('dashboardActivities', svc);
}(window.angular));
