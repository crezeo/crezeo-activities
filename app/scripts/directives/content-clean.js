(function ($ng) {
  'use strict';

  var directive = ['$compile', '$parse', function($compile, $parse) {

    //  Return usable function/link inside
    //  directive to avoid error while compile
    //
    //*************************************
    return {
      restrict: 'E',
      link: function(scope, element, attr) {
        scope.$watch(attr.content, function() {
          element.html($parse(attr.content)(scope));
          $compile(element.contents())(scope);
        }, true);
      }
    };

  }];


  $ng.module('crezeo-activities').directive('contentClean', directive);

}(window.angular));
