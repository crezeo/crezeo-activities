// myConfig.js
exports.config = {
  seleniumAddress: 'http://localhost:9515',
  //specs:['test/protractor/*.spec.js', 'test/protractor/**/*.spec.js', 'test/protractor/**/**/*.spec.js'], //relativepathfromconfigfile
  suites: {
    homepage: 'test/protractor/tokster/homepage/*.spec.js',
    signup: 'test/protractor/tokster/signup/*.spec.js'
  },

  multiCapabilities: [
//    {
//      'browserName': 'firefox'
//    },
    {
      'browserName': 'phantomjs'
    }
  ],
  onPrepare: function() {
// implicit and page load timeouts
    browser.manage().timeouts().pageLoadTimeout(40000);
    browser.manage().timeouts().implicitlyWait(25000);

    // for non-angular page
    browser.ignoreSynchronization = true;

  }

  // --params.login.user 'ngrocks'
  // Used with :
  //var params = browser.params;
  //params.login.user
  //params.login.password

  //params: {
  //  login: {
  //    user: 'protractor-br',
  //    password: '#ng123#'
  //  }
  //}
};
