/// Add models to models service to be injected into angular application.
(function ($app, $ng) {
  'use strict';
  var svc = [function () {

    var models = {};
    for (var m in window.CREZEO_APP.models) {
      if (window.CREZEO_APP.models.hasOwnProperty(m)) {
        models[m] = window.CREZEO_APP.models[m];
      }
    }

    return {
      models: models
    };
  }];

  $ng.module('crezeo-server-api')
    .factory('models', svc);

}(window.CREZEO_APP, window.angular));
