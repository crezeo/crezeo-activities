// myConfig.js
var hri = require('human-readable-ids').hri;
var randomBuildName = 'Tokster - ' + hri.random();
exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
//  sauceUser: "Nomaki",
//  sauceKey: "30cf49c1-6f33-4637-bd43-9054610a4845",
  //specs:['test/protractor/*.spec.js', 'test/protractor/**/*.spec.js', 'test/protractor/**/**/*.spec.js'], //relativepathfromconfigfile
  suites: {
    homepage: 'test/protractor/tokster/homepage/*.spec.js',
    signup: 'test/protractor/tokster/signup/*.spec.js',
    editor: 'test/protractor/tokster/editor/*.spec.js',
    institution: 'test/protractor/tokster/institution/*.spec.js'
  },

  multiCapabilities: [
    //{
    //  'browserName': 'internet explorer',
    //  'platform': 'Windows 7',
    //  'version': '10.0',
    //  'name': 'Tokster Internet Explorer',
    //  'build': randomBuildName,
    //  shardTestFiles: true,
    //  maxInstances: 2
    //},
//    {
//      'browserName': 'chrome',
//      'name': 'Tokster Chrome',
//      'platform': 'Windows 8.1',
//      'build': randomBuildName,
//      shardTestFiles: true,
//      maxInstances: 2
//    }
    {'browserName': 'chrome'}
  ],
  onPrepare: function() {
// implicit and page load timeouts
    browser.manage().timeouts().pageLoadTimeout(40000);
    browser.manage().timeouts().implicitlyWait(25000);

    // for non-angular page
    browser.ignoreSynchronization = true;

  },
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 60000,
    isVerbose : true,
    includeStackTrace : true
  }

  // --params.login.user 'ngrocks'
  // Used with :
  //var params = browser.params;
  //params.login.user
  //params.login.password

  //params: {
  //  login: {
  //    user: 'protractor-br',
  //    password: '#ng123#'
  //  }
  //}
};
