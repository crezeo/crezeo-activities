(function ($ng) {
  'use strict';
  var ctrl = ['milkyWayConf', '$scope', 'pubsub', '$log', 'api', '$state', 'dashboardActivities','discoverActivities', '$timeout', 'suggestions', '$filter', '$http', '$rootScope', 'config', 'loader','$element', function (milkyWayConf, $scope, pubsub, $log, api, $state, dashboardActivities,discoverActivities, $timeout, suggestions, $filter, $http, $rootScope, config, loader,$element) {
    /*

     ActivitiesCtrl
     ======
     > Handles the activities and suggestions getting and manipulation for all stream inclusions


     Accessible methods in scope
     ------
     ** loadMore **  Get more activities (for infinite scroll purpose)

     Local methods
     ------
     ** getSuggestions **  Get suggestions related to connected user
     ** getTags **  Get universes childs (if crezeo-universes is used)
     ** loadActivities **  Get activities related to connected user
     ** changeNumberOfFollower **  Manipulate the number of followers of universeschilds (if crezeo-universes is used)

     PubSub Events
     ------
     ### Subscribe
     ** suggestion-item-removed **  Remove a suggestion item from scope and push a refused notification to API
     ** suggestion-bloc-removed **  Remove a suggestion bloc from scope and push a suggestion hide to API
     ** suggestion-bloc-change **  When a new bloc has to be displayed after a refuse
     ** statusSaving **  Reload activities when a status has been saved
     ** reloadActivities **  Reload activities
     ** reloadItems **  Reload activities (ToDo : needs refactoring)
     ** tags.newfollowers **  When a universe child got a new follower, trigger changeNumberOfFollower (if crezeo-universes is used)
     ** tags.newUnfollowers **  When a universe child lose a follower, trigger changeNumberOfFollower (if crezeo-universes is used)

     ### Publish
     ** suggestion-bloc-empty **  Published if a suggestion bloc has no more items to show (after some refuse for eg)

     */


    // ********* Declarations *********

    $scope.suggestionRejected = [];
    $scope.suggestions = [];
    $scope.suggestionHides = [];

    // Make the controller 'busy' to avoid infinite scroll to trigger yes
    $scope.loading = true;
    $scope.loadingTags = false;
    $scope.page = 1;
    $scope.busy = true;
    $scope.posting = false;

    $scope.suggestionResponsiveIntro = [
//      {
//        breakpoint: 768,
//        settings: {
//          slidesToShow: 2
//        }
//      },
      {
        breakpoint: 479,
        settings: {
          slidesToShow: 1
        }
      }
    ];



    // ********* Local methods *********

    function getTags() {
      // Get temporarly a suggestion of tags
      $scope.loadingTags = true;
      api.tags.search({'scope': 'universes_childs', 'child': true, with_follow: false, order: 'score', without_hightligths: true})
        .then(function (response) {
          $scope.universes_childs = response.data;
          $scope.loadingTags = false;
        });
    }

    function getSuggestions() {
      var dataId;
      if ($scope.consultedUniverse) {
        dataId = $scope.consultedUniverse.id;
      }
      suggestions.get($scope.suggestionRejected, 10,dataId).then(function (data) {
        $scope.suggestions = $scope.suggestions.concat(data);
        addSuggestToActivity();
        $scope.suggestionRejected = suggestions.getRejectedBlocId($scope.suggestions);
//        $scope.suggestionRejectedType = suggestions.getRejectedType($scope.suggestions)
//        $scope.suggestionRejectedField = suggestions.getRejectedField($scope.suggestions)

      });
    }
    function addSuggestToActivity(){
      if($scope.activities){
        for (var i = 0; i < $scope.activities.length; i++) {
          if($scope.showSuggestion(i)){
            $scope.activities[i].haveSuggest = true
          }
        }
      }
    }

    var loadActivities = function (force_reload, keepSuggestBloc,filterName) {
      force_reload = force_reload || undefined;
      filterName = filterName || undefined;
      loader.start('activities');
      var dataId = ($state.current.data && $state.current.data.discover)  ? undefined : $rootScope.$user.id;
      if ($scope.consultedUniverse) {
        dataId = $scope.consultedUniverse.id;
      }
      var activitiesService = ($state.current.data && $state.current.data.discover) ? discoverActivities.get(dataId, force_reload, filterName) : dashboardActivities.get(dataId, force_reload);
      activitiesService.then(function (activities) {
        if (keepSuggestBloc) {
          $scope.keepSuggestBloc = true
        }
        $scope.activities = [];
        for (var i = 0; i < activities.length; i++) {
          var act = $filter('filter')($scope.activities, {id: activities[i].id}, true)[0];
          if(!act){
            $scope.activities.push(activities[i]);
          }
        }
        if ($scope.activities && $scope.activities.length == 0) {
          getTags()
        }
        $timeout(function () {
          $scope.busy = false;
          loader.stop('activities');
        }, 2000);
        addSuggestToActivity();
        $scope.loading = false;
        $scope.posting = false;
      })
    };

    function changeNumberOfFollower(targetId, number) {
      if(milkyWayConf.modules['crezeo-universes']) {
        if ($scope.universes_childs && $scope.universes_childs.length > 0) {
          var tag = $filter('filter')($scope.universes_childs, {id: targetId}, true)[0];
          if (tag) {
            tag.number_of_followers = tag.number_of_followers + number;
          }
        }
      }
    }

    changeNumberOfFollower();

    // ********* Scope methods *********
    $scope.loadMore = function () {
      if ($scope.busy === true || $scope.disableInfinite === true) {
        return;
      }
      else {
        // Say that the infinite scroll couldn't be triggered while busy
        $scope.busy = true;
        loader.start('activities');
        getSuggestions();

        var dataId = ($state.current.data && $state.current.data.discover) ? undefined : $rootScope.$user.id;

        if ($scope.consultedUniverse) {
          dataId = $scope.consultedUniverse.id;
        }
        var activitiesService = ($state.current.data && $state.current.data.discover) ? discoverActivities.get(dataId, undefined, $scope.currentFilter, $scope.page + 1) : api.users.get($scope.$user.id, 'current_user_activities?page=' + ($scope.page + 1));
        activitiesService.then(function (response) {
          if(response.data){
            var data = response.data;
          }else{
            var data = response;
          }
          if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
              var act = $filter('filter')($scope.activities, {id: data[i].id}, true)[0];
              if(!act){
                $scope.activities.push(data[i]);
              }
            }
            addSuggestToActivity();
            $scope.page = $scope.page + 1;
          } else {
            $scope.disableInfinite = true;
            $scope.activitiesEnd = true;
          }
          loader.stop('activities');
          //When activities are got, release the infinite scroll

//          $timeout(function () {
            $scope.busy = false;
//          }, 200);
        });

      }
    };

    $scope.showSuggestion = function (index, activity_id) {
      return suggestions.showSuggestion(index, $scope.suggestions)
    };

    // ********* Events *********

    pubsub.subscribe('suggestion-item-removed', function (event, data) {
      var matchesParent = $filter('filter')($scope.suggestions, {bloc_type: data.blocType}, true);
      var matchParent = matchesParent[0];
      var suggestionIndex = $scope.suggestions.indexOf(matchParent);
      var itemIndex = $scope.suggestions[suggestionIndex].items.indexOf(data.item);
//      $scope.suggestions[suggestionIndex].items.splice(itemIndex, 1);
//      $scope.suggestions[suggestionIndex].items[itemIndex].hidden = true;
      if (!data.accept) {
        var action = new api.suggestion_actions({action: 'refused', target_type: data.blocType, target_id: data.item.id, bloc_id: data.bloc_id})
        action.save()
          .then(function (response) {
          })
      }
      if ($scope.suggestions[suggestionIndex].items.length == 0) {
        pubsub.publish('suggestion-bloc-empty', {blocType: data.bloc_id});
      }
      if ($scope.suggestions[suggestionIndex].items.length >= 3) {
//        $http.post(window.API_URL + "/api/v1/suggestion_actions/view", {bloc_type: data.blocType, bloc_id: data.bloc_id, item_ids: [$scope.suggestions[suggestionIndex].items[2].id], avoid_bloc_save: true});
      }
    }, $scope);

    pubsub.subscribe('suggestion-bloc-removed', function (event, data) {
      $scope.suggestionHides.push(data);
    }, $scope);

    pubsub.subscribe('suggestion-bloc-change', function (event, data) {
      var bloc = $filter('filter')($scope.suggestions, {bloc_id: parseInt(data.bloc_id)}, true)[0];
      var suggestionIndex = $scope.suggestions.indexOf(bloc);
      var itemIndex = $scope.suggestions[suggestionIndex] = data.new_bloc;
      $scope.suggestionRejected = suggestions.getRejectedBlocId($scope.suggestions);
    }, $scope);

    pubsub.subscribe('statusSaving', function () {
      $scope.busy = true;
      $scope.posting = true;
      loadActivities(true);
    }, $scope);

    pubsub.subscribe('reloadActivities', function (event, data) {
      $scope.busy = true;
      loadActivities(true, true, data);
    }, $scope);

    pubsub.subscribe('reloadItems', function (event, data) {
      $scope.busy = true;
      loadActivities(true, data);
    }, $scope);

    if(milkyWayConf.modules['crezeo-universes']) {
      pubsub.subscribe("tags.newfollowers", function (event, targetId) {
        changeNumberOfFollower(targetId, 1);
      }, $scope);

      pubsub.subscribe("tags.newUnfollowers", function (event, targetId) {
        changeNumberOfFollower(targetId, -1);
      }, $scope);
    }


    // ********* Launchers *********

    //redirect to home if user is not authenticated
    //if (!config.get('user') && !milkyWayConf.network.haveHomeDiscover) {
    //  $state.go('log-in');
    //  return;
    //}

    getSuggestions();

      if ($rootScope.forceReloadActivities) {
        loadActivities(true);
        $rootScope.forceReloadActivities = undefined;
      } else {
        loadActivities();
      }
  }];

  $ng.module('crezeo-activities').controller('ActivitiesCtrl', ctrl);
}(window.angular));
