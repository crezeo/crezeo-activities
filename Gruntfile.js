// Generated on 2014-02-10 using generator-angular 0.7.1
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Define the configuration for all the tasks
  grunt.initConfig({
    protractor_webdriver: {
      options: {
        path: '/Users/romain/Sites/webapp-tokster/bin/'
      },
      your_target: {
        // Target-specific file lists and/or options go here.
      }
    },
    protractor: {
      options: {
        configFile: "protractorConfig.js", // Default config file
        keepAlive: false, // If false, the grunt process stops when the test fails.
        noColor: false, // If true, protractor will not use colors in its output.
        args: {
          sauceUser: "Nomaki",
          sauceKey: "30cf49c1-6f33-4637-bd43-9054610a4845"
        }
      },
      your_target: {   all: {} }// Grunt requires at least one target to run so you can simply put 'all: {}' here too.
    },
    //
    // Project settings
    yeoman: {
      // configurable paths
      app: require('./bower.json').appPath || 'app',
      dist: 'dist'
    },

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      js: {
        files: ['<%= yeoman.app %>/scripts/{,**/}*.js'],
        tasks: ['newer:jshint:all'],
        options: {
          livereload: false
        }
      },
      jsTest: {
        files: ['test/spec/{,**/}*.js'],
        tasks: ['newer:jshint:test', 'karma']
      },
      //styles: {
      //  files: ['<%= yeoman.app %>/themes/*/styles/{,**/}*.css'],
      //  tasks: ['newer:copy:styles', 'autoprefixer']
      //},
      gruntfile: {
        files: ['Gruntfile.js']
      }
    },

    // The actual grunt server settings
    connect: {
      options: {
        port: 9000,
        // Change this to '0.0.0.0' to access the server from outside.
        hostname: '0.0.0.0'
      },
//      proxies: [{
//          context: '/api',
//          host: 'tokster.p3v2.crezeo.com',
//          port: 80,
//          changeOrigin: true,
//          rejectUnauthorized: false
//        },
//        {
//          context: '/media_files',
//          host: 'tokster.p3v2.crezeo.com',
//          port: 80,
//          changeOrigin: true,
//          rejectUnauthorized: false
//        },
//        {
//          context: '/system',
//          host: 'tokster.p3v2.crezeo.com',
//          port: 80,
//          changeOrigin: true,
//          rejectUnauthorized: false
//        }],
      livereload: {
        options: {
          open: true,
          base: [
            '.tmp',
            '<%= yeoman.app %>'
          ],
          // Added by Antoine to work with a proxy (using the staging API server)
          // Install : npm install grunt-connect-proxy --save-dev
//          middleware: function (connect, options) {
//            if (!Array.isArray(options.base)) {
//              options.base = [options.base];
//            }
//
//            // Setup the proxy
//            var middlewares = [require('grunt-connect-proxy/lib/utils').proxyRequest];
//
//            // Serve static files.
//            options.base.forEach(function(base) {
//              middlewares.push(connect.static(base));
//            });
//
//            // Make directory browse-able.
//            var directory = options.directory || options.base[options.base.length - 1];
//            middlewares.push(connect.directory(directory));
//
//            return middlewares;
//          }
        }
      },
      test: {
        options: {
          port: 9001,
          base: [
            '.tmp',
            'test',
            '<%= yeoman.app %>'
          ]
        }
      },
      dist: {
        options: {
          base: '<%= yeoman.dist %>'
        }
      }
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: [
        'Gruntfile.js'
        //'<%= yeoman.app %>/scripts/{,**/}*.js'
      ],
      test: {
        options: {
          jshintrc: 'test/.jshintrc'
        },
        src: ['test/spec/{,*/}*.js']
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= yeoman.dist %>/*',
            '!<%= yeoman.dist %>/.git*'
          ]
        }]
      },
      server: '.tmp'
    },

    //// Add vendor prefixed styles
    //autoprefixer: {
    //  options: {
    //    browsers: ['last 1 version']
    //  },
    //  dist: {
    //    files: [{
    //      expand: true,
    //      cwd: '.tmp/styles/',
    //      src: '{,**/}*.css',
    //      dest: '.tmp/styles/'
    //    }]
    //  }
    //},

    // Automatically inject Bower components into the app
    'bower-install': {
      app: {
        html: '<%= yeoman.app %>/index.html',
        ignorePath: '<%= yeoman.app %>/',
        // ignored because this file needs to be injected before angularjs
        exclude: ['<%= yeoman.app %>/bower_components/ng-file-upload/angular-file-upload-shim.js']
      }
    },





    // Renames files for browser caching purposes
    rev: {
      dist: {
        files: {
          src: [
            '<%= yeoman.dist %>/scripts/{,**/}*.js',
            //'<%= yeoman.dist %>/themes/*/styles/{,**/}*.css',
            '<%= yeoman.dist %>/images/{,**/}*.{png,jpg,jpeg,gif,webp,svg}',
            '<%= yeoman.dist %>/styles/fonts/*'
          ]
        }
      }
    },

    // Reads HTML for usemin blocks to enable smart builds that automatically
    // concat, minify and revision files. Creates configurations in memory so
    // additional tasks can operate on them
    useminPrepare: {
      html: '<%= yeoman.app %>/index.html',
      options: {
        dest: '<%= yeoman.dist %>'
      }
    },

    // Performs rewrites based on rev and the useminPrepare configuration
    usemin: {
      html: ['<%= yeoman.dist %>/{,**/}*.html'],
      options: {
        assetsDirs: ['<%= yeoman.dist %>', '<%= yeoman.dist %>/images']
      }
    },

    // The following *-min tasks produce minified files in the dist folder
    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/images',
          src: '{,**/}*.{png,jpg,jpeg,gif}',
          dest: '<%= yeoman.dist %>/images'
        }]
      }
    },
    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/images',
          src: '{,*/}*.svg',
          dest: '<%= yeoman.dist %>/images'
        }]
      }
    },
    htmlmin: {
      dist: {
        options: {
          collapseWhitespace: true,
          collapseBooleanAttributes: true,
          removeCommentsFromCDATA: true,
          removeOptionalTags: true
        },
        files: [{
          expand: true,
          cwd: '<%= yeoman.dist %>',
          src: ['*.html', 'themes/*/views/{,**/}*.html'],
          dest: '<%= yeoman.dist %>'
        }]
      }
    },

    //compress: {
    //  dist: {
    //    options: {
    //      mode: 'gzip'
    //    },
    //    files: [
    //      // Each of the files in the src/ folder will be output to
    //      // the dist/ folder each with the extension .gz.js
    //      {expand: true, cwd: '<%= yeoman.dist %>', src: ['scripts/*.js'], dest: '<%= yeoman.dist %>', ext: '.js.gz', extDot: 'last'}
    //    ]
    //  }
    //},




    // Allow the use of non-minsafe AngularJS files. Automatically makes it
    // minsafe compatible so Uglify does not destroy the ng references
    ngmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/concat/scripts',
          src: '*.js',
          dest: '.tmp/concat/scripts'
        }]
      }
    },

    // Replace Google CDN references
    cdnify: {
      dist: {
        html: ['<%= yeoman.dist %>/*.html']
      }
    },

    // Copies remaining files to places other tasks can use
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= yeoman.app %>',
          dest: '<%= yeoman.dist %>',
          src: [
            '*.{ico,png,txt}',
            '.htaccess',
            '*.html',
            'version.json',
            'scripts/{,**/}*.html',
            'themes/*/views/{,**/}*.html',
            'themes/{,**/}*',
            //'bower_components/**/*',
            'images/{,**/}*.{webp}',
            'fonts/*'
          ]
        },{
          // HACK(brent): get select2 images into necessary dist folder
          expand: true,
          cwd: '<%= yeoman.app %>/bower_components/select2',
          dest: '<%= yeoman.dist %>/styles',
          src: ['*.png']
        },{
          expand: true,
          cwd: '<%= yeoman.app %>/styles/font-icons/custom-icons/font',
          dest: '<%= yeoman.dist %>/font',
          src: ['*.{eot,svg,ttf,woff}']
        }, {
          expand: true,
          cwd: '.tmp/images',
          dest: '<%= yeoman.dist %>/images',
          src: ['generated/*']
        }]
      }
      //styles: {
      //  expand: true,
      //  cwd: '<%= yeoman.app %>/styles',
      //  dest: '.tmp/styles/',
      //  src: '{,**/}*.css'
      //}
    },

    // Run some tasks in parallel to speed up the build process
    concurrent: {
      server: [
      ],
      test: [
      ],
      dist: [
        'imagemin',
        'svgmin'
      ]
    },

    // By default, your `index.html`'s <!-- Usemin block --> will take care of
    // minification. These next options are pre-configured if you do not wish
    // to use the Usemin blocks.
    // cssmin: {
    //   dist: {
    //     files: {
    //       '<%= yeoman.dist %>/styles/main.css': [
    //         '.tmp/styles/{,*/}*.css',
    //         '<%= yeoman.app %>/styles/{,*/}*.css'
    //       ]
    //     }
    //   }
    // },
    // uglify: {
    //   dist: {
    //     files: {
    //       '<%= yeoman.dist %>/scripts/scripts.js': [
    //         '<%= yeoman.dist %>/scripts/scripts.js'
    //       ]
    //     }
    //   }
    // },
    // concat: {
    //   dist: {}
    // },

    // Test settings
    karma: {
      unit: {
        configFile: 'karma.conf.js',
        singleRun: true
      }
    }
  });

  grunt.registerTask('serve', function (target) {
    if (target === 'dist') {
      return grunt.task.run(['build', 'connect:dist:keepalive']);
    }

    grunt.task.run([
      'clean:server',
      'bower-install',
      'configureProxies',
      'concurrent:server',
      'connect:livereload',
      'watch'
    ]);
  });

  grunt.registerTask('server', function () {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run(['serve']);
  });

  grunt.registerTask('test', [
    'clean:server',
    'concurrent:test',
    'connect:test',
    'protractor'
  ]);

  grunt.registerTask('dist-only', [
    'build'
  ]);

  grunt.registerTask('build', [
    'clean:dist',
    'bower-install',
    'useminPrepare',
    'concurrent:dist',
    'concat',
    'ngmin',
    'copy:dist',
    'cdnify',
    'cssmin',
    'uglify',
    'rev',
    'usemin',
    'htmlmin',
    'compress:dist'
  ]);

  grunt.registerTask('default', [
    'test',
    'build'
  ]);
  grunt.loadNpmTasks('grunt-contrib-compress');
};
