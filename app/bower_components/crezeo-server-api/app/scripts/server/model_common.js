(function (Class) {
  'use strict';

// Common functions for models (refactoring)
  window.crCreateModel = function(name, model, cache_name) {
    model.init = function (params) {
      for (var p in params) {
        if (params.hasOwnProperty(p)) {
          this[p] = params[p];
        }
      }
      this._resource = name;
      this._cache_name = cache_name;
    };

    var ext = window.Class.extend(model);
    // adding the resource name after the model is extended allows us to use a name other than the api name.  e.g. user_sessions vs sessions
    ext._resource = name;
    ext._cache_name = cache_name;
    return ext;
  };

}(window.Class));
